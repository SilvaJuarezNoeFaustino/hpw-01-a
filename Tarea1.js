// creación del grupo

grupo={
    
    "clave":"1234",
    "nombre":"ISA"
    
    "docente":{
        "clave":"2345",
        "nombre":"Antonio",
        "apellidos":"Hernández Blas",
        "grado_academico":"Licenciado"
    },
    
    "materias":[
        {"clave":"1e2e","nombre":"Herramientas de Prog. Web"},
        {"clave":"3a4a","nombre":"Prog. Lógica y Funcional"},
        {"clave":"5d6d","nombre":"Administración de BD"}
        
    ],
    
    "alumnos":[
        {"clave":"09161299",
        "nombre":"Noé Faustino",
        "apellidos":"Silva Juárez",
        
        "calificaciones":[
            {
            "clave_mat":"5d6d",
            "clave_alum":"09161299",
            "calificacion":"80"
            }
            {
            "clave_mat":"1e2e",
            "clave_alum":"09161299",
            "calificacion":"85"
            }
        ],
        },
        
        
        {"clave":"09161242",
        "nombre":"Hector Gregorio",
        "apellidos":"Morales Pacheco",
        
        "calificaciones":[
            {
            "clave_mat":"5d6d",
            "clave_alum":"09161299",
            "calificacion":"80"
            }
            {
            "clave_mat":"1e2e",
            "clave_alum":"09161299",
            "calificacion":"85"
            }
        ]
        
        },
        ]
    
    
    
};

function num_alumnos1(grupo){
    if(grupo.alumnos){
        return grupo.alumnos.length;
    }
return 0;

}

/*Llamamos a la funcion e imprima en consola */
console.log(num_alumnos1(grupo));

function updateAlumno(clave,nombre){
            if (grupo["alumnos"]){
                grupo.alumnos[grupo.alumnos.length]={"clave":clave,"nombre":nombre}    
             return  grupo.alumnos;
            }
         return 0;
        }

function deleteAlumno(clave,nombre){
        for(var i=0; i < grupo.alumnos.length ; i++){
               if(grupo.alumnos[i]["clave"] == clave){
                   grupo.alumnos.splice(i,1);
                   return grupo.alumnos;
               }
           }
           return 0;
        }        